package com.agt;

import java.io.IOException;
import java.util.Properties;

import com.agt.constants.Constants;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;

/***
 * 
 * @author mjnasir
 *
 */
@SuppressWarnings("restriction")
public class Main {
//
	static final String BASE_URI = "http://localhost:8001/agt/";
	public static void main(String[] args) {

		try {
			int [] is = {1};
			loadConfigurations();
			HttpServer server = HttpServerFactory.create(BASE_URI);
			server.start();
			System.out.println("Press any key to stop server");
			System.in.read();
			server.stop(0);

		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	public static void loadConfigurations() throws IOException {
		Properties properties = new Properties();
		properties.load(Main.class.getClassLoader().getResourceAsStream(Constants.CONFIG_FILE));
		Constants.LARGE_FILE_THRESHOLD = Integer.parseInt(properties.getProperty(Constants.LARGE_FILE));
		Constants.WORDS_FREQUENCY = Integer.parseInt(properties.getProperty(Constants.WORDS_OCCURENCE));
	}

}

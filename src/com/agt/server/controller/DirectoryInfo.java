package com.agt.server.controller;

import javax.ws.rs.core.Response;

public interface DirectoryInfo {
	
	Response getDirectoryInfo(String path) throws Exception ;
	
}

package com.agt.server.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import com.agt.server.model.ErrorMessage;
import com.agt.server.model.JsonModel;
import com.agt.server.services.DirectoryProcessService;


@Path("/directory_info")
public class DirectoryInfoImpl implements DirectoryInfo {
	
	private DirectoryProcessService service;
	
	public DirectoryInfoImpl(){
		service = new DirectoryProcessService();
	}
	
	@GET
	@Path("/{path}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.TEXT_PLAIN)
	@Override
	public Response getDirectoryInfo(@PathParam("path") String path) throws Exception{

		service = new DirectoryProcessService();
		JsonModel jsonModel = null;
		Response response= null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			jsonModel = service.process(path);
			response = Response.status(200).entity(mapper.writeValueAsString(jsonModel)).build();
		} catch (Exception ex) {
			response = Response.status(500).entity(mapper.writeValueAsString(new ErrorMessage(500,ex.getMessage()))).build();
//			ex.printStackTrace();
		}
		
 		return response;
	}

}

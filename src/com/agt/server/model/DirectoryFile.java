package com.agt.server.model;

import java.io.File;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class DirectoryFile {

	private String fileName;
	private Integer totalCount;
	@JsonInclude(Include.NON_NULL)
	private Map<String, Integer> wordCounts;

	public DirectoryFile() {
	}

	public String getFileName() {
		return fileName;
	}

	public DirectoryFile(File file) {
		this.fileName = file.getName();
	}

	public DirectoryFile(String fileName) {
		this.fileName = fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Map<String, Integer> getWordCounts() {
		return wordCounts;
	}

	public void setWordCounts(Map<String, Integer> wordCounts) {
		this.wordCounts = wordCounts;
	}
}
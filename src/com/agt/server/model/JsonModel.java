package com.agt.server.model;

public class JsonModel {
	private Directory largeFiles;
	private Directory smallFiles;

	public JsonModel() {

	}

	public Directory getLargeFiles() {
		return largeFiles;
	}

	public void setLargeFiles(Directory largeFiles) {
		this.largeFiles = largeFiles;
	}

	public Directory getSmallFiles() {
		return smallFiles;
	}

	public void setSmallFiles(Directory smallFiles) {
		this.smallFiles = smallFiles;
	}

}

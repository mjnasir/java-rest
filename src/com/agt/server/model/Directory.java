package com.agt.server.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Directory {
	@JsonInclude(Include.NON_NULL)
	private List<DirectoryFile> files;
	@JsonInclude(Include.NON_NULL)
	private List<Directory> directories;
	private String directoryName;
	
	public Directory(){
	}
	
	public List<DirectoryFile> getFiles() {
		return files;
	}

	public void setFiles(List<DirectoryFile> files) {
		this.files = files;
	}

	public List<Directory> getDirectories() {
		return directories;
	}

	public void setDirectories(List<Directory> directories) {
		this.directories = directories;
	}

	public String getDirectoryName() {
		return directoryName;
	}

	public void setDirectoryName(String directoryName) {
		this.directoryName = directoryName;
	}

}

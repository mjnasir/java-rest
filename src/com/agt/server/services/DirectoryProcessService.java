package com.agt.server.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.agt.constants.Constants;
import com.agt.server.model.Directory;
import com.agt.server.model.DirectoryFile;
import com.agt.server.model.JsonModel;

public class DirectoryProcessService {
	/***
	 * 
	 * @param folder
	 * @param smallFileDirectory
	 * @param largeFileDirectory
	 * @throws Exception
	 * 
	 */

	public DirectoryProcessService() {

	}

	public JsonModel process(String path) throws Exception {

		Directory small = new Directory();
		Directory large = new Directory();
		if (!new File(path).exists()){
			throw new FileNotFoundException(Constants.INVALID_PATH);
		}

		try{
			traverse(new File(path), small, large);
		}catch(Exception ex){
			throw new Exception(Constants.ERROR_PROCESSING_FILES);
		}
		JsonModel jsonModel = new JsonModel();
		jsonModel.setLargeFiles(large);
		jsonModel.setSmallFiles(small);
		return jsonModel;

	}

	private void traverse(File folder, Directory smallFileDirectory, Directory largeFileDirectory)
			throws FileNotFoundException, IOException {

		if (!folder.isDirectory() || !folder.exists()){
			throw new FileNotFoundException(Constants.NOT_FOLDER);
		}

		File allTextFiles[] = folder.listFiles((dir,name) -> {return name.endsWith(Constants.TXT_FILE);});
		File subDirectories[] = folder.listFiles((dir,name) -> {return new File(dir, name).isDirectory();});
		smallFileDirectory.setDirectoryName(folder.getName());
		largeFileDirectory.setDirectoryName(folder.getName());

		if (subDirectories.length > 0) {
			smallFileDirectory.setDirectories(new LinkedList<>());
			largeFileDirectory.setDirectories(new LinkedList<>());
			for (File dir : subDirectories) {
				Directory tempSmall = new Directory();
				Directory tempLarge = new Directory();
				traverse(dir, tempSmall, tempLarge);
				smallFileDirectory.getDirectories().add(tempSmall);
				largeFileDirectory.getDirectories().add(tempLarge);
			}
		}

		if (allTextFiles.length > 0) {
			smallFileDirectory.setFiles(new LinkedList<>());
			largeFileDirectory.setFiles(new LinkedList<>());
			Arrays.asList(allTextFiles).parallelStream()
			.forEach(f -> processFile(f, smallFileDirectory.getFiles(), largeFileDirectory.getFiles()));
		}
	}

	private void processFile(File f, List<DirectoryFile> smallFilesDirectory, List<DirectoryFile> largeFilesDirectory) {

		DirectoryFile directoryFile = new DirectoryFile(f);
		int totalWords = count(f);
		directoryFile.setTotalCount(totalWords);
		if (totalWords > Constants.LARGE_FILE_THRESHOLD) {
			Map<String, Integer> wordCount = getWordCount(f);
			wordCount = wordCount.entrySet().stream()
					.filter(p -> p.getValue() >= Constants.WORDS_FREQUENCY && !p.getKey().trim().isEmpty())
					.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
			directoryFile.setWordCounts(wordCount);
			largeFilesDirectory.add(directoryFile);
		} else {
			smallFilesDirectory.add(directoryFile);
		}
	}

	private int count(File file){
		int count = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = br.readLine()) != null) {
				count += line.split("\\s").length;
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return -1;
		}
		return count;

	}

	private Map<String, Integer> getWordCount(final File f) {
		String line;
		Map<String, Integer> wordCountMap = new HashMap<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(f))) {
			while ((line = reader.readLine()) != null) {
				if (!"".equalsIgnoreCase(line.trim())) {
					for (String word : Arrays.asList(line.trim().replace("\\W", "").split("\\s+"))) {
						Integer freq = wordCountMap.containsKey(word.toLowerCase())
								? wordCountMap.get(word.toLowerCase()) + 1 : 1;
						wordCountMap.put(word.toLowerCase(), freq);
					}
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return wordCountMap;
	}

}

package com.agt.constants;

public class Constants {
	public final static String TXT_FILE = ".txt";
	public final static String LARGE_FILE = "LARGE_FILE_THRESHOLD";
	public final static String WORDS_OCCURENCE = "WORDS_OCCURENCE";
	public final static String NOT_FOLDER = "Not a Folder";
	public final static String CONFIG_FILE = "application.properties";
	public final static String ERROR_PROCESSING_FILES = "Error processing files!";
	public static Integer LARGE_FILE_THRESHOLD;
	public static final String INVALID_PATH = "Invalid Directory Path";
	public static Integer WORDS_FREQUENCY;
}
